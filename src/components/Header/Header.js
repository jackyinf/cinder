import React from 'react'
import { IndexLink, Link } from 'react-router'
import './Header.scss'

import {Navbar, NavItem , Nav, Col, Image} from 'react-bootstrap';
import icon from './assets/icon.png';

export const Header = () => (
  <div>
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="/">Cinder</a>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav pullRight>
        <NavItem eventKey={1} href="#">Welcome, Nidhogg!</NavItem>
        <NavItem eventKey={1} href="#">
          <Col xs={6} md={4}>
            <Image src={icon} circle className='header--icon' />
          </Col>
        </NavItem>
      </Nav>
    </Navbar>
  </div>
);

export default Header
