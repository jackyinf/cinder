/**
 * Created by zekar on 9/21/2016.
 */

import React from 'react';
import './DragonProfile.scss';
import {Link} from 'react-router';

class DragonProfile extends React.Component {
  render() {
    const { image, name, description, id, dbId, } = this.props.dragon;
    const link = this.props.showLink ? (<div>
      <hr />
      <Link to={`/dragon/${id}?dbId=${dbId}&lookingFor=${this.props.lookingFor}`} className="btn btn-success">Go to details</Link>
    </div>) : (<div></div>);

    return (<figure className="snip1559">
      <div className="profile-image">
        <img src={image} alt="profile-sample2"  />
      </div>
      <figcaption>
        <h3>{name}</h3>
        <hr />
        <p>{description}</p>
        {link}
      </figcaption>
    </figure>)
  }
}

DragonProfile.proptypes = {
  dragon: React.PropTypes.object.isRequired,
  lookingFor: React.PropTypes.string.isRequired,
  showLink: React.PropTypes.bool.isRequired
};

export default DragonProfile;
