import { combineReducers } from 'redux'

export const makeRootReducer = (asyncReducers) => {
  if (!asyncReducers)
    return combineReducers({base: () => 0});  // avoid undefined
  return combineReducers({
    ...asyncReducers
  })
};

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers))
};

export default makeRootReducer
