import { connect } from 'react-redux';
import { getSingleAsync, toggleLoading } from '../modules/dragon';
import Dragon from '../components/Dragon';

const mapDispatchToProps = { getSingleAsync, toggleLoading };

const mapStateToProps = (state) => {
  return {
    dragon : state.dragon,
    isLoading: state.isLoading
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dragon)
