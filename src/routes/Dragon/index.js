import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'dragon/:id',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const DragonContainer = require('./containers/DragonContainer').default;
      const {dragonReducer, loadingReducer} = require('./modules/dragon');

      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'dragon', reducer: dragonReducer });
      injectReducer(store, { key: 'isLoading', reducer: loadingReducer });

      cb(null, DragonContainer);

    }, 'dragon')
  }
})
