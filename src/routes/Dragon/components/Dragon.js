import React from 'react'
import './Dragon.scss';
import {Button, Grid, Row, Col} from 'react-bootstrap';
import { browserHistory } from 'react-router';
import DragonProfile from './../../../components/DragonProfile';

class Dragon extends React.Component {
  constructor() {
    super();

    this.toAshes = this.toAshes.bind(this);
    this.lightTheFlame = this.lightTheFlame.bind(this);

    this.state = {dbId: undefined, lookingFor: ''};
  }

  async componentDidMount() {
    const id = this.props.params['id'];
    const {lookingFor, dbId} = this.props.location.query;
    this.setState({dbId, lookingFor});
    await this.props.getSingleAsync(id);
  }

  static isEmpty(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }

  toAshes() {
    window.firebase.child(`dragons/${this.state.dbId}`).remove();
    browserHistory.push(`/dragonList?lookingFor=${this.state.lookingFor}`);
  }

  lightTheFlame() {
    window.firebase.child(`dragons/${this.state.dbId}`).remove();
    window.firebase.child('dragons').push(this.props.dragon);
    browserHistory.push(`/dragonList?lookingFor=${this.state.lookingFor}`);
  }

  render() {
    const loadingDiv = (<div>Is loading</div>);
    const emptyDiv = (<div>None</div>);

    return (<div style={{ margin: '0 auto' }} >
      {this.props.isLoading
        ? (loadingDiv) : Dragon.isEmpty(this.props.dragon) ? emptyDiv :
        (<Grid>
          <Row>
            <DragonProfile dragon={this.props.dragon} showLink={false} lookingFor={this.state.lookingFor} />
          </Row>

          <Row>
            <Col md={12}>
              <ul className="ch-grid">
                <li>
                  <div className="ch-item img-ashes">
                    <div className="ch-info" onClick={this.toAshes}>
                      <h3>To ashes</h3>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="ch-item img-flame">
                    <div className="ch-info" onClick={this.lightTheFlame}>
                      <h3>Light the flame!</h3>
                    </div>
                  </div>
                </li>
              </ul>
            </Col>
          </Row>
        </Grid>)}
    </div>);
  }
}

Dragon.propTypes = {
  getSingleAsync   : React.PropTypes.func.isRequired,
  toggleLoading    : React.PropTypes.func.isRequired
};

export default Dragon
