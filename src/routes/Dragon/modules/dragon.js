import fetch from 'isomorphic-fetch';

// ------------------------------------
// Constants
// ------------------------------------
export const SET_SINGLE = 'SET_SINGLE';
export const TOGGLE_LOADING = 'TOGGLE_LOADING';

// ------------------------------------
// Actions
// ------------------------------------
export const getSingleAsync = (id) => (dispatch, getState) => {
  return new Promise((resolve) => {
    fetch(`http://www.dragonsofmugloar.com/dating/api/profile/${id}`)
      .then(response => response.json())
      .then(response => {
        dispatch(setSingle(response));
        dispatch(toggleLoading(false));
        resolve();
      })
  });
};

export function toggleLoading (value) {
  return {
    type    : TOGGLE_LOADING,
    payload : value
  }
}

export function setSingle (value) {
  return {
    type    : SET_SINGLE,
    payload : value
  }
}

export const actions = {
  getSingleAsync,
  toggleLoading
};

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS_FOR_DRAGON = {
  [SET_SINGLE] : (state, action) => {
    return action.payload;
  }
};

const ACTION_HANDLERS_FOR_LOADING = {
  [TOGGLE_LOADING] : (state, action) => {
    return action.payload;
  }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialStateForDragon = {};
export function dragonReducer (state = initialStateForDragon, action) {
  const handler = ACTION_HANDLERS_FOR_DRAGON[action.type];
  return handler ? handler(state, action) : state
}

const initialStateForLoading = {};
export function loadingReducer (state = initialStateForLoading, action) {
  const handler = ACTION_HANDLERS_FOR_LOADING[action.type];
  return handler ? handler(state, action) : state
}
