// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import Home from './Welcome'
import DragonListRoute from './DragonList'
import DragonRoute from './Dragon';

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  : Home,
  childRoutes : [
    DragonListRoute(store),
    DragonRoute(store)
  ]
});

export default createRoutes
