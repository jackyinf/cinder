import React from 'react'
import image_male from '../assets/male.png';
import image_female from '../assets/female.jpg';
import './HomeView.scss'

import {Grid, Row, Col, Image} from 'react-bootstrap'
import { Link } from 'react-router';
import { browserHistory } from 'react-router';

// export const HomeView = () => ();
export class HomeView extends React.Component {

  gotoDragonList(whom) {
    browserHistory.push(`/dragonList?lookingFor=${whom}`);
  }

  render() {
    return (<Grid>
      <Row>
        <h4 className="who-do-you">Who do you</h4>
      </Row>
      <Row>
        <Col md={12}>
          <ul className="ch-grid">
            <li>
              <div className="ch-item ch-img-1">
                <div className="ch-info" onClick={e => this.gotoDragonList('girl')}>
                  <h3>Interested in Girls?</h3>
                  <p>Check them out at <Link href="/dragonList?lookingFor=girl">Here</Link></p>
                </div>
              </div>
            </li>
            <li>
              <div className="ch-item ch-img-2">
                <div className="ch-info" onClick={e => this.gotoDragonList('boy')}>
                  <h3>Interested in Boys?</h3>
                  <p>Check them out at <Link href="/dragonList?lookingFor=boy">Here</Link></p>
                </div>
              </div>
            </li>
          </ul>
        </Col>
      </Row>
      {/*<Row>*/}
      {/*<Col xs={6} md={2} mdOffset={4}>*/}
      {/*<Link href="/dragonList?lookingFor=girl">*/}
      {/*<Image width="100" src={image_female} circle />*/}
      {/*</Link>*/}
      {/*</Col>*/}
      {/*<Col xs={6} md={2}>*/}
      {/*<Link href="/dragonList?lookingFor=boy">*/}
      {/*<Image width="100" src={image_male} circle />*/}
      {/*</Link>*/}
      {/*</Col>*/}
      {/*<Col md={4}></Col>*/}
      {/*</Row>*/}
      <Row>
        <h4 className="want-to-see">want to see?</h4>
      </Row>
    </Grid>);
  }
}

export default HomeView
