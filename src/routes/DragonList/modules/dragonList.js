/**
 * Created by zekar on 9/19/2016.
 */

import fetch from 'isomorphic-fetch';

// ------------------------------------
// Constants
// ------------------------------------
export const SET_POTENTIALS = 'SET_POTENTIALS';
export const SET_MATCHES = 'SET_MATCHES';
export const TOGGLE_LOADING = 'TOGGLE_LOADING';

// ------------------------------------
// Actions
// ------------------------------------
export const getPotentialsAsync = (lookingFor) => async (dispatch, getState) => {

  // We prepare function to fetch from and return as a json.
  var genderQuery = lookingFor === "boy" || lookingFor === "girl" ? `?gender=${lookingFor}` : "";
  const initFetch = () => fetch(`http://www.dragonsofmugloar.com/dating/api/profile/random${genderQuery}`)
    .then(resp => resp.json());

  // We initialize n-times the fetch to get items one-by-one
  const promises = [];
  const nrOfPromises = 4;
  for (let i = 0; i < nrOfPromises; i++) {
    promises.push(initFetch());
  }

  // We wait for all the fetches and get all responses into a single array.
  // var list = await Promise.all(promises);
  // dispatch(setPotentials(list));
  return Promise.all(promises).then(list => {
    dispatch(setPotentials(list));
  })
};

export const getMatchesAsync = (lookingFor) => async (dispatch, getState) => {

  // Get all matches from firebase (I named them simply and confusingly 'dragons')
  var dragons = window.firebase.child('dragons');
  var matches = []; // prepared list

  // Get all matches from firebase
  await dragons.once("value", function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      var key = childSnapshot.key();
      var childData = childSnapshot.val();
      const likesYou = 'likesYou' in childData && childData['likesYou'] === true;
      const genderOk = !lookingFor || lookingFor !== 'girl' || lookingFor !== 'boy' || 'gender' in childData && childData['gender'] === lookingFor;
      if (likesYou && genderOk) {
        matches.push(Object.assign({dbId: key}, childData));
      }
    });
  });

  // Set matches
  dispatch(setMatches(matches));
  dispatch(toggleLoading(false));
};

export function setPotentials (value) {
  return {
    type    : SET_POTENTIALS,
    payload : value
  }
}

export function setMatches (value) {
  return {
    type    : SET_MATCHES,
    payload : value
  }
}

export function toggleLoading (value) {
  return {
    type    : TOGGLE_LOADING,
    payload : value
  }
}

export const actions = {
  getPotentialsAsync,
  getMatchesAsync,

  setPotentials,
  setMatches,
  toggleLoading
};

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS_FOR_POTENTIALS = {
  [SET_POTENTIALS] : (state, action) => (action.payload)
};
const ACTION_HANDLERS_FOR_LOADING = {
  [TOGGLE_LOADING] : (state, action) => (action.payload)
};
const ACTION_HANDLERS_FOR_MATCHES = {
  [SET_MATCHES] : (state, action) => (action.payload)
};

// ------------------------------------
// Reducer
// ------------------------------------
export function potentialsReducer(state = [], action) {
  const handler = ACTION_HANDLERS_FOR_POTENTIALS[action.type];
  return handler ? handler(state, action) : state
}
export function matchesReducer(state = [], action) {
  const handler = ACTION_HANDLERS_FOR_MATCHES[action.type];
  return handler ? handler(state, action) : state
}
export function loadingReducer(state = true, action) {
  const handler = ACTION_HANDLERS_FOR_LOADING[action.type];
  return handler ? handler(state, action) : state
}
