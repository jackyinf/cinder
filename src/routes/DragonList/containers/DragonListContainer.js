/**
 * Created by zekar on 9/19/2016.
 */

import { connect } from 'react-redux';
import { getPotentialsAsync, getMatchesAsync, setPotentials, setMatches, toggleLoading } from '../modules/dragonList';
import DragonListView from '../components/DragonListView';

const mapDispatchToProps = { getPotentialsAsync, getMatchesAsync, setPotentials, setMatches, toggleLoading  };
const mapStateToProps = (state) => ({
  isLoading: state.isLoading,
  potentials: state.potentials,
  matches: state.matches
});

export default connect(mapStateToProps, mapDispatchToProps)(DragonListView)
