/**
 * Created by zekar on 9/19/2016.
 */

import React from 'react';
import image_heart from './../assets/heart.png';
import './DragonListView.scss';
import {Grid, Row, Col, Image} from 'react-bootstrap';
import DragonProfile from './../../../components/DragonProfile';

class DragonListView extends React.Component {
  constructor() {
    super();
    this.hasMatches = this.hasMatches.bind(this);
    this.state = {lookingFor: ''};
  }

  async componentDidMount() {
    const lookingFor = this.props.location.query["lookingFor"];
    this.setState({lookingFor});
    await this.props.getPotentialsAsync(lookingFor);
    await this.props.getMatchesAsync(lookingFor);
  }

  hasMatches() {
    return this.props.matches && this.props.matches.length > 0;
  }

  render() {
    const dragonsListFn = (list) => (list.map(item => (<Col md={3} key={item.id}>
      <DragonProfile dragon={item} showLink={true} lookingFor={this.state.lookingFor} />
      </Col>)));

    return (<Grid>
      <Row>
        <Col md={12} className="meet-your-flames">
          <div className="alert alert-info" role="alert">
            Meet your flames, invite them to pottery class or yoga. Don't mess up the kingdom.
          </div>
        </Col>
      </Row>
      <Row>
        <Row>
          <h3>Potential dates. Pick one!</h3>
        </Row>
        <Row className="well">
          {this.props.isLoading ? (<h4>Loading list...</h4>) : (dragonsListFn(this.props.potentials))}
        </Row>
      </Row>

      {this.hasMatches() ?
        (<Row>
          <Row>
            <div className="matches-title">
              <h3>
                <img src={image_heart} width={50} />
                Matches
                <img src={image_heart} width={50} />
              </h3>
            </div>
          </Row>
          <Row className="well">
            {dragonsListFn(this.props.matches)}
          </Row>
        </Row>)
        : (<div></div>)}
    </Grid>)
  }
}

DragonListView.propTypes = {
  getPotentialsAsync   : React.PropTypes.func.isRequired,
  getMatchesAsync    : React.PropTypes.func.isRequired,
  setPotentials    : React.PropTypes.func.isRequired,
  setMatches    : React.PropTypes.func.isRequired,
  toggleLoading    : React.PropTypes.func.isRequired
};

export default DragonListView;
