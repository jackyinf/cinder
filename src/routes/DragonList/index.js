/**
 * Created by zekar on 9/19/2016.
 */

import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'dragonList',

  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {

    /*  Webpack - use 'require.ensure' to create a split point and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {

      /*  Webpack - use require callback to define dependencies for bundling   */
      const DragonList = require('./containers/DragonListContainer').default;
      const {potentialsReducer, matchesReducer, loadingReducer} = require('./modules/dragonList');

      /*  Add the reducer to the store on key  */
      injectReducer(store, { key: 'potentials', reducer: potentialsReducer });
      injectReducer(store, { key: 'matches', reducer: matchesReducer });
      injectReducer(store, { key: 'isLoading', reducer: loadingReducer });

      /*  Return getComponent   */
      cb(null, DragonList);

      /* Webpack named bundle   */
    }, 'dragonList')
  }
})
