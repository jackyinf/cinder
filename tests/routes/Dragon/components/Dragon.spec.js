import React from 'react'
import { bindActionCreators } from 'redux'
import Dragon from 'routes/Dragon/components/Dragon'
import { shallow } from 'enzyme'

describe('(Component) Dragon', () => {
  let _props, _spies, _wrapper;

  beforeEach(() => {
    _spies = {};
    _props = {
      counter : 5,
      ...bindActionCreators({
        getSingleAsync : (_spies.getSingleAsync = sinon.spy()),
        toggleLoading   : (_spies.toggleLoading = sinon.spy())
      }, _spies.dispatch = sinon.spy())
    };
    _wrapper = shallow(<Dragon {..._props} />)
  });

  it('Should render as a <div>.', () => {
    expect(_wrapper.is('div')).to.equal(true)
  });

  it('Should render with an <h2> that includes Sample Dragon text.', () => {
    expect(_wrapper.find('h2').text()).to.match(/Dragon:/);  // it will fail :P
  });
});
